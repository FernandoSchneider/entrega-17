# Users CRUD (with JWT authentication)

*Flask Factory REST API* with JWT authorization and authentication that **Create, Read, Update and Delete** users from a *postgresql* database.

## Getting Started!

The first thing first is to create a `.env` file with your informations of data base and a secret key.

After that is necessary execute the following commands:

```
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

To **create and activate** a virtual enviroment and install **all dependencies** for the project!

```
flask db upgrade
```

To use migrations and create all tables in the database specified in the `.env` file.

## Endpoints:

**POST**  -  `/api/signup`

Create a new user, accept the fallowing - *EXEMPLE* - json body:
```JSON
{
    "name": "Joe",
    "last_name": "Doe",
    "email": "joedoe@mail.com",
    "password": "joe-doe123"
}
```

and return:
```JSON
{
    "name": "Joe",
    "last_name": "Doe",
    "email": "joedoe@mail.com"
}
```
**POST**  -  `/api/signin`

Login using the user email and password:
```JSON
{
    "email": "joedoe@mail.com",
    "password": "joe-doe123"  
}
```
Should return a JWT token if everything is correct:
```JSON
{
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmcmVzaCI6ZmFsc2UsImlhdCI6MTYzMzQ0MzkwMSwianRpIjoiMjhjNDU2M2UtMzllYy00ZmRmLThjOWMtNDc3YmE3MDkzYTdiIiwidHlwZSI6ImFjY2VzcyIsInN1YiI6NiwibmJmIjoxNjMzNDQzOTAxLCJleHAiOjE2MzM0NDQ4MDF9.n_LC_PiG6hWfTP1i9EfOwsnKEzLDAJaIN9vwY97NSb8"
}
```
**GET**  -  `/api`

*Bearer Token required for request.*

Read and return all informations for the *current user* logged:
```JSON
{
    "name": "Joe",
    "last_name": "Doe",
    "email": "joedoe@mail.com"
}
```
**PUT** - `/api`

*Bearer Token required for request*

Update and return all informations for the *current user* logged.

Info to be updated:
```JSON
{
    "name": "Joe 3",
    "last_name": "Doe 3",
    "email": "joedoe3@mail.com",
    "password": "joe-doe123456"
}
```
Return the user with his info updated:
```JSON
{
    "name": "Joe 3",
    "last_name": "Doe 3",
    "email": "joedoe3@mail.com"
}
```
**DELETE** - `/api`

*Bearer Token required for request*

Delete and return the user deleted for the *current user* logged.

Return a message with the user deleted:
```JSON
{
    "msg": "User Joe 3 has been deleted."
}
```