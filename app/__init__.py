from flask import Flask
from app.configs import database, migration, enviroment, jwt
from app import blueprints

def create_app():

    app = Flask(__name__)

    enviroment.init_app(app)
    database.init_app(app)
    migration.init_app(app)
    jwt.init_app(app)
    blueprints.init_app(app)

    return app