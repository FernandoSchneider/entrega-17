from flask import request
from flask import jsonify
from app.models.user_model import UserModel
from http import HTTPStatus
from flask_jwt_extended import (
    create_access_token, 
    jwt_required,
    current_user
)
from app.configs.jwt import jwt

@jwt.user_identity_loader
def _user_identity_lookup(user):
    return user.id

@jwt.user_lookup_loader
def _user_lookup_callback(_jwt_header, jwt_data):
    identity = jwt_data["sub"]

    return UserModel.query.filter_by(id=identity).one_or_none()

def sign_up():
    data_json = request.get_json()
    password = data_json.pop('password')

    user = UserModel(**data_json)
    user.password = password

    user.save()

    return jsonify(user), HTTPStatus.OK

def sign_in():
    email = request.json.get('email')
    password = request.json.get('password')

    user = UserModel.query.filter_by(email=email).first()

    if not user:
        return jsonify(msg='User not found!'), HTTPStatus.NOT_FOUND

    if user.verify_password(password):
        access_token = create_access_token(identity=user)

        return jsonify(access_token=access_token), HTTPStatus.OK
    
    else:
        return jsonify(msg='Unathorized'), HTTPStatus.UNAUTHORIZED

@jwt_required()
def get_user_info():
    user = current_user

    return user.format_self(), HTTPStatus.OK

@jwt_required()
def update_user_info():
    user = current_user
    update_data = request.get_json()

    for key, value in update_data.items():
        setattr(user, key, value)

    user.save()

    return user.format_self(), HTTPStatus.OK

@jwt_required()
def delete_user_info():
    user = current_user

    user.delete()

    return jsonify(msg=f'User {user.name} has been deleted.'), HTTPStatus.OK
