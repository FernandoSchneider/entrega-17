from flask import Blueprint
from app.controllers.user_controller import (
    sign_up, 
    sign_in, 
    get_user_info, 
    update_user_info,
    delete_user_info
)

bp_user = Blueprint('user_bp', __name__, url_prefix='/api')

bp_user.post('/signup')(sign_up)
bp_user.post('/signin')(sign_in)
bp_user.get('')(get_user_info)
bp_user.put('')(update_user_info)
bp_user.delete('')(delete_user_info)
