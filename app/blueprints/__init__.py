from flask import Flask
from . import user_blueprint

def init_app(app: Flask):
    app.register_blueprint(user_blueprint.bp_user)